## Ecn
brew install minikube

minikube start

minikube addons enable registry
docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"

docker build -t localhost:5000/mytestimage .
docker push localhost:5000/mytestimage

kubectl apply -f ./k8s/namesapce.yml
kubectl apply -f ./k8s/dep.yml & kubectl apply -f ./k8s/serv.yml

minikube tunnel

kubectl get services -n my-name-space fastapi-service

*
cat /etc/hosts
## Added by Docker Desktop
## To allow the same kube context to work on the host and the container:
#127.0.0.1 kubernetes.docker.internal
minikube addons enable ingress

mkcert -key-file key.pem -cert-file cert.pem kubernetes.docker.internal

kubectl apply -f ./k8s/star/local-ingress.yml

minikube tunnel
