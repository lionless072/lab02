# Развёртывание локальго кубера

## Установка локального k8s (minikube)
```bash
brew install minikube
```

## запуск minikube
```bash
minikube start
```

## Запускаем minikube dashboard, чтобы получить визуальный интерфейс локального кубера
```bash
minikube addons enable dashboard
minikube dashboard
```

## Создаём "витруальное" хранилище образов для докера на порту 5000
```bash
minikube addons enable registry
docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"
```

# Развёртывание приложения
## Реализуем простой сервис на питоне
![Исходный_код](docs/source_code.png)

## Описываем все зависимости в requirements.txt
![Зависимости](docs/reqs.png)

## Создаём докерфайл для создания докер-образа приложения. Важно учесть, что приложение будет запущено на порту 8000
![Докер](docs/docker.png)

## Собираем докер-образ


```bash
docker build -t localhost:5000/mytestimage .
docker push localhost:5000/mytestimage
```
## Создаём манифесты 
Манифесты хранятся в ./k8s. Для задания неоходимы три манифеста: 
 - Namespace
 - Deployment
 - Service

![namespace](docs/k8s_namespace.png)
![deployment](docs/k8s_deployment.png)
![service](docs/k8s_service.png)


## Создаём объекты в k8s
```bash
kubectl apply -f ./k8s/namesapce.yml
kubectl apply -f ./k8s/dep.yml & kubectl apply -f ./k8s/serv.yml
```

## Для того, чтобы посмотреть результат работы сервиса, необходимо развернуть маршрут до сервисов:
```bash
minikube tunnel
```

## Проверяем, что сервис работает:
![Hello_world](docs/hello_world.png)

# Задание со звёздочкой
В рамках этого задания необходимо создать ingress с использованием самоподписного сертификата. Для этого неоходимо проверить, что для класетра k8s есть запись в  /etc/hosts 
![hosts](docs/hosts.png)
для локального хоста уже сущетсвует запись с именем хоста kubernetes.docker.internal


## Создаём самоподписный сертификат c помощью простой утилиты mkcert
```bash
mkcert -key-file key.pem -cert-file cert.pem kubernetes.docker.internal
```
В резуьтате сгенерированы два файла  cert.pem и key.pem


## На основе созданных файлов создаём TLS-секрет
```bash
kubectl create secret tls test-tls --key="key.pem" --cert="cert.pem" -n my-name-space
```

## Создаём Ingress, поддерживающий tls подключение
![ingress](docs/k8s_ingress.png)

## Устанавливаем ingress-аддон
```bash
minikube addons enable ingress
```

## Деплоим ingress
```bash
kubectl apply -f ./k8s/star/local-ingress.yml
```

## Проверяем результат
Вводим в адреcной строке браузера kubernetes.docker.internal. В результате получаем ошибку о невозможности проверить сертификат.

![cert](docs/cert.png)
